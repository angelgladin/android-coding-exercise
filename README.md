# Android Take Home Coding Exercise

Android application developed as coding exercise following the requirements in the given [specification](/Android Take Home Project.pdf).
The goal of the sample is to showcase a list of contact display them, see a contact and being able to modify a contact information.

This app uses the latest Android Components such as [Jetpack Compose](https://developer.android.com/jetpack/compose), [Navigation Components](https://developer.android.com/jetpack/compose/navigation), [Compose Material 3](https://developer.android.com/jetpack/compose/designsystems/material3), and [ViewModels](https://developer.android.com/codelabs/basic-android-kotlin-compose-viewmodel-and-state#0).

It's worth mentioning that a [3rd party library is used for parsing the CSV file](https://github.com/doyaaaaaken/kotlin-csv).

This csv file was [used](app/src/main/java/assets/sample_contacts.csv).

### Assumptions

- The contacts list is persisted only in memory.
- There is no internal database for storing the data, meaning whenever a contact is modified, the content will return to its original state when the app is closed and opened again.
- No error handling for retrieving the contacts, **the csv file will always be available in the application**.


# References
- https://blog.canopas.com/keyboard-handling-in-jetpack-compose-all-you-need-to-know-3e6fddd30d9a
- https://developer.android.com/jetpack/compose/navigation
- https://bignerdranch.com/blog/using-the-navigation-component-in-jetpack-compose/


![](art/showcase.mp4)
