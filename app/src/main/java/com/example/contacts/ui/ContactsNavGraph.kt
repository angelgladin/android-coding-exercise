@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.contacts.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.contacts.R
import com.example.contacts.ui.components.ContactsAppBar

private interface Destination {
    val route: String
    val title: Int
}

object Home : Destination {
    override val route: String = "home"
    override val title: Int = R.string.app_name
}

object Detail : Destination {
    override val route: String = "detail"
    override val title: Int = R.string.detail
    private const val contactIdArg = "id"
    val routeWithArg: String = "$route/{$contactIdArg}"
    val arguments = listOf(navArgument("id") { type = NavType.IntType })
    fun getNavigationRouteToEdit(id: Int) = "$route/$id"
}

object Edit : Destination {
    override val route: String = "edit"
    override val title: Int = R.string.edit
    private const val contactIdArg = "id"
    val routeWithArg: String = "$route/{$contactIdArg}"
    val arguments = listOf(navArgument("id") { type = NavType.IntType })
    fun getNavigationRouteToEdit(id: Int) = "$route/$id"
}


@Composable
fun ContactsNavGraph(
    viewModel: ContactsViewModel = viewModel(),
    navController: NavHostController = rememberNavController()
) {
    val context = LocalContext.current

    LaunchedEffect(Unit) {
        viewModel.fetchContacts(context)
    }

    val backStackEntry by navController.currentBackStackEntryAsState()

    Scaffold(
        topBar = {
            val routePrefix = backStackEntry?.destination?.route?.substringBefore('/') ?: ""
            val screenName = when (routePrefix) {
                Home.route -> stringResource(Home.title)
                Detail.route -> stringResource(Detail.title)
                Edit.route -> stringResource(Edit.title)
                else -> ""
            }
            ContactsAppBar(
                currentScreen = screenName,
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() }
            )
        }
    ) { innerPadding ->

        NavHost(
            navController = navController,
            startDestination = Home.route,
            modifier = Modifier.padding(innerPadding)
        ) {
            composable(route = Home.route) {
                HomeScreen(viewModel,
                    onClickedContact = { id ->
                        navController.navigate(
                            Detail.getNavigationRouteToEdit(
                                id
                            )
                        )
                    })
            }
            composable(
                route = Detail.routeWithArg,
                arguments = Detail.arguments
            ) { backStackEntry ->
                val contactId = backStackEntry.arguments?.getInt("id") ?: -1
                ContactScreen(
                    viewModel,
                    contactId,
                    {
                        navController.navigate(Edit.getNavigationRouteToEdit(contactId))
                    },
                    false
                )
            }
            composable(
                route = Edit.routeWithArg,
                arguments = Edit.arguments
            ) { backStackEntry ->
                val contactId = backStackEntry.arguments?.getInt("id") ?: -1
                ContactScreen(
                    viewModel,
                    contactId,
                    {
                        navController.popBackStack(Home.route, false)
                    },
                    true
                )
            }

        }
    }

}

