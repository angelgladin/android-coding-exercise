@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.contacts.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.contacts.R
import com.example.contacts.data.Contact

@Composable
fun ContactScreen(
    viewModel: ContactsViewModel,
    contactId: Int,
    onClickButton: () -> Unit,
    isEditing: Boolean
) {
    val contact = viewModel.getContact(contactId)

    val firstName = remember { mutableStateOf(contact.firstName) }
    val lastName = remember { mutableStateOf(contact.lastName) }
    val companyName = remember { mutableStateOf(contact.companyName) }
    val address = remember { mutableStateOf(contact.address) }
    val city = remember { mutableStateOf(contact.city) }
    val county = remember { mutableStateOf(contact.county) }
    val state = remember { mutableStateOf(contact.state) }
    val zip = remember { mutableStateOf(contact.zip) }
    val phone = remember { mutableStateOf(contact.phone) }
    val phone1 = remember { mutableStateOf(contact.phone1) }
    val email = remember { mutableStateOf(contact.email) }

    val scrollState = rememberScrollState()

    Box(
        modifier = Modifier
            .padding(horizontal = 12.dp)
            .fillMaxSize()

    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .verticalScroll(scrollState)
        ) {
            LabeledText(title = stringResource(R.string.first_name), enabled = isEditing, state = firstName)
            LabeledText(title = stringResource(R.string.last_name), enabled = isEditing, state = lastName)
            LabeledText(title = stringResource(R.string.company_name), enabled = isEditing, state = companyName)
            LabeledText(title = stringResource(R.string.address), enabled = isEditing, state = address)
            LabeledText(title = stringResource(R.string.city), enabled = isEditing, state = city)
            LabeledText(title = stringResource(R.string.county), enabled = isEditing, state = county)
            LabeledText(title = stringResource(R.string.state), enabled = isEditing, state = state)
            LabeledText(title = stringResource(R.string.zip), enabled = isEditing, state = zip)
            LabeledText(title = stringResource(R.string.phone), enabled = isEditing, state = phone)
            LabeledText(title = stringResource(R.string.phone), enabled = isEditing, state = phone1)
            LabeledText(title = stringResource(R.string.email), enabled = isEditing, state = email)
        }

        FloatingActionButton(modifier = Modifier
            .padding(16.dp)
            .align(Alignment.BottomEnd),
            onClick = {
                if (isEditing) {
                    viewModel.update(
                        contactId,
                        Contact(
                            id = contactId,
                            firstName = firstName.value,
                            lastName = lastName.value,
                            companyName = companyName.value,
                            address = address.value,
                            city = city.value,
                            county = county.value,
                            state = state.value,
                            zip = zip.value,
                            phone = phone.value,
                            phone1 = phone.value,
                            email = email.value,
                        )
                    )
                }

                onClickButton()
            }) {

            Icon(
                imageVector = if (isEditing) Icons.Default.Done else Icons.Default.Edit,
                contentDescription = null
            )
        }
    }
}

@Composable
fun LabeledText(
    modifier: Modifier = Modifier,
    title: String,
    enabled: Boolean,
    state: MutableState<String>
) {
    OutlinedTextField(
        modifier = modifier
            .fillMaxWidth()
            .padding(vertical = 5.dp),
        value = state.value,
        maxLines = 1,
        onValueChange = { state.value = it },
        label = {
            Text(text = title)
        },
        readOnly = !enabled
    )
}