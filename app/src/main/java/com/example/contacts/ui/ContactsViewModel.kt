package com.example.contacts.ui

import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.contacts.data.Contact
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class ContactsViewModel : ViewModel() {
    private val _contactsList = MutableStateFlow(mutableListOf<Contact>())
    val contactsList: StateFlow<MutableList<Contact>> = _contactsList.asStateFlow()
    fun fetchContacts(context: Context) {
        if (_contactsList.value.isNotEmpty()) return

        val csvString =
            context.assets.open("sample_contacts.csv").bufferedReader().use { it.readText() }
        val csvContent = csvReader().readAll(csvString).drop(1)
        val newLIst =
            csvContent.mapIndexed { index, x ->
                Contact(index, x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10])
            }.toMutableList()
        _contactsList.update { newLIst }
    }

    fun update(id: Int, contact: Contact) {
        _contactsList.update {
            it[id] = contact.copy(id = id)
            it
        }
    }

    fun getContact(index: Int) : Contact = _contactsList.value[index]
}