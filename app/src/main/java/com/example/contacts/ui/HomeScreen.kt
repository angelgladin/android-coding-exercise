package com.example.contacts.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.contacts.data.Contact

@Composable
fun HomeScreen(
    viewModel: ContactsViewModel,
    onClickedContact: (Int) -> Unit,
    modifier: Modifier = Modifier
) {
    val contactsList by viewModel.contactsList.collectAsState()

    LazyColumn() {
        items(contactsList) { contact ->
            ContactItem(contact, onClickedContact)
        }
    }
}

@Composable
fun ContactItem(contact: Contact, onClick: (Int) -> Unit) {
    val shape = RoundedCornerShape(20.dp)

    ElevatedCard(
        modifier = Modifier
            .padding(horizontal = 12.dp, vertical = 4.dp)
            .fillMaxWidth()
            .clip(shape)
            .clickable(onClick = { onClick.invoke(contact.id) }),
        shape = shape
    ) {
        Row(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            val backgroundColor = MaterialTheme.colorScheme.primary
            val contentColor = MaterialTheme.colorScheme.onPrimary

            Box(
                modifier = Modifier
                    .size(38.dp)
                    .background(
                        shape = CircleShape,
                        color = backgroundColor
                    )
            ) {
                val firstLetter = contact.firstName[0].toString()
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    color = contentColor,
                    text = firstLetter,
                    fontSize = 16.sp
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            Text(text = "${contact.firstName} ${contact.lastName}")
        }
    }
}